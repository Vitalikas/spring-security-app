//package lt.kolinko.spring.security.utils;
//
//import lt.kolinko.spring.security.entities.Role;
//import lt.kolinko.spring.security.entities.User;
//import org.hibernate.SessionFactory;
//import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
//import org.hibernate.cfg.Configuration;
//import org.hibernate.cfg.Environment;
//import org.hibernate.service.ServiceRegistry;
//
//import java.util.Properties;
//
//public class HibernateUtil {
//    private static SessionFactory sessionFactory;
//
//    public static SessionFactory getSessionFactory() {
//        if (sessionFactory == null) {
//            try {
//                // Hibernate settings equivalent to hibernate.cfg.xml's properties
//                Properties properties = new Properties();
//                properties.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
//                properties.put(Environment.URL, "jdbc:mysql://localhost:3306/springsecurity?serverTimezone=UTC");
//                properties.put(Environment.USER, "root");
//                properties.put(Environment.PASS, "Dosia1983");
//                properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
//                properties.put(Environment.SHOW_SQL, "true");
//                properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
//                properties.put(Environment.HBM2DDL_AUTO, "update");
//
//                Configuration configuration = new Configuration();
//                configuration.setProperties(properties);
//                configuration.addAnnotatedClass(Role.class);
//                configuration.addAnnotatedClass(User.class);
//                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
//                        .applySettings(configuration.getProperties()).build();
//                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return sessionFactory;
//    }
//
//    public static void shutdown() {
//        // Close caches and connection pools
//        getSessionFactory().close();
//    }
//}
