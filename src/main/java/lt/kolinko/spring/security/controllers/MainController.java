package lt.kolinko.spring.security.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

// Principle is current user's information saved in Spring security context (for authenticated users)

@RestController
public class MainController {
    // access for any users
    @GetMapping("/")
    public String homePage() {
        return "home";
    }

    // access for authenticated users
    @GetMapping("/authenticated")
    public String pageForAuthenticatedUsers(Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(authentication.getAuthorities());
        return "secured part of web service. User name: " + principal.getName();
    }

    @GetMapping("/profile")
    public String pageForReadProfile(Principal principal) {
        return "read profile page. Hello: " + principal.getName();
    }

    @GetMapping("/admin")
    public String pageForAdmins(Principal principal) {
        return "admins page. Hello: " + principal.getName();
    }
}
