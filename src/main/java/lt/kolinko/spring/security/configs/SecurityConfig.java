package lt.kolinko.spring.security.configs;

import lt.kolinko.spring.security.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/authenticated/**").authenticated()
                .antMatchers("/admin/**").hasAuthority("ADMIN")
                .antMatchers("/profile/**").hasAnyRole("ADMIN", "USER")
                .and()
                .formLogin()
                .and()
                .logout().logoutSuccessUrl("/");
    }



    // In-memory users generator and authentication
//    @Bean
//    public UserDetailsService users() {
//        // UserDetails is info about user. Is it possible to build user using User.builder(). ...
//        UserDetails user = User.builder()
//                .username("user")
//                .password("{bcrypt}$2y$12$cxeoYPgZwlYcPmLdpf0lreeSBvF6dq/zDpVFX8VH9cdySWD/bTHqK")
//                .roles("USER")
//                .build();
//        UserDetails admin = User.builder()
//                .username("admin")
//                .password("{bcrypt}$2y$12$cxeoYPgZwlYcPmLdpf0lreeSBvF6dq/zDpVFX8VH9cdySWD/bTHqK")
//                .roles("ADMIN", "USER")
//                .build();
//        return new InMemoryUserDetailsManager(user, admin);
//    }

    // jdbcAuthentication
//    @Bean
//    public JdbcUserDetailsManager users(DataSource dataSource) {
//        UserDetails user2 = User.builder()
//                .username("user2")
//                .password("{bcrypt}$2y$12$cxeoYPgZwlYcPmLdpf0lreeSBvF6dq/zDpVFX8VH9cdySWD/bTHqK")
//                .roles("USER")
//                .build();
//        UserDetails admin2 = User.builder()
//                .username("admin2")
//                .password("{bcrypt}$2y$12$cxeoYPgZwlYcPmLdpf0lreeSBvF6dq/zDpVFX8VH9cdySWD/bTHqK")
//                .roles("ADMIN", "USER")
//                .build();
//        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager(dataSource);
//        if (jdbcUserDetailsManager.userExists(user2.getUsername())) {
//            jdbcUserDetailsManager.deleteUser(user2.getUsername());
//        }
//        if (jdbcUserDetailsManager.userExists(admin2.getUsername())) {
//            jdbcUserDetailsManager.deleteUser(admin2.getUsername());
//        }
//        jdbcUserDetailsManager.createUser(user2);
//        jdbcUserDetailsManager.createUser(admin2);
//
//        return jdbcUserDetailsManager;
//    }

    // encodes entered passwords from plain text to hash
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider userAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(userService);

        return daoAuthenticationProvider;
    }
}
