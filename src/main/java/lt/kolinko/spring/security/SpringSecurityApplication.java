package lt.kolinko.spring.security;

import lt.kolinko.spring.security.entities.Role;
import lt.kolinko.spring.security.entities.User;
//import lt.kolinko.spring.security.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Collections;

@SpringBootApplication
public class SpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityApplication.class, args);

//		Transaction transaction = null;
//		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
//			transaction = session.beginTransaction();
//			User user1 = new User("username1", "$2y$12$6KBIrhwC6HnN2JOPkwbDUeDnabtb6GvbgyctXYg4LzsTO8u.PiEhO", "email1");
//			User user2 = new User("username2", "$2y$12$6KBIrhwC6HnN2JOPkwbDUeDnabtb6GvbgyctXYg4LzsTO8u.PiEhO", "email2");
//			Role role1 = new Role("ADMIN");
//			Role role2 = new Role("USER");
//
//			user1.setRoles(Arrays.asList(role1, role2));
//			user2.setRoles(Collections.singletonList(role2));
//
//			saveAll(session, Arrays.asList(user1, user2));
//			saveAll(session, Arrays.asList(role1, role2));
//
//			transaction.commit();
//		} catch (Exception e) {
//			if (transaction != null) {
//				transaction.rollback();
//				e.printStackTrace();
//			}
//			e.printStackTrace();
//		}
//
//		// shutting down
//		HibernateUtil.shutdown();
	}

//	private static <T> void saveAll(Session session, Iterable<T> entities) {
//		entities.forEach(session::saveOrUpdate);
//	}
}
